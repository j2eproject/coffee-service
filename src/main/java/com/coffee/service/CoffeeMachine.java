package com.coffee.service;

import com.coffee.service.model.EditUrl;
import com.coffee.service.model.Product;
import com.coffee.service.service.ProductService;
import com.coffee.service.service.ProductServiceImpl;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import kong.unirest.HttpRequestWithBody;
import kong.unirest.Unirest;
import kong.unirest.json.JSONArray;
import kong.unirest.json.JSONObject;
import spark.Spark;

import java.io.FileInputStream;
import java.util.*;

import static spark.Spark.delete;
import static spark.Spark.get;
import static spark.Spark.options;
import static spark.Spark.post;
import static spark.Spark.put;

public class CoffeeMachine {
    private static int counter = 0;
    private static int freq = 3000;
    private static String serverURL = "http://127.0.0.1";
    private static int serverPort = 8080;
    private static int agentPort = 7575;

    public static void main(String[] args) {
        readConf();
        Spark.port(agentPort);
        Spark.before("*", (req, res) -> {
            System.out.println("Request " + req.requestMethod() + " done on " + req.uri() + " from IP :" + req.ip());
        });

        final ProductService productService = new ProductServiceImpl();

        get("/version", (request, response) -> {
            return "1.1";
        });

        get("/config", (request, response) -> {
            return serverURL + ":" + serverPort;
        });

        post("/config", (request, response) -> {
            EditUrl newUrl = new Gson().fromJson(request.body(), EditUrl.class);
            serverPort = newUrl.getPort();
            serverURL = newUrl.getUrl();
            System.out.println("new server conf : " + serverURL + ":" + serverPort);
            response.status(200);
            return response;
        });

        get("/products", (request, response) -> {
            response.type("application/json");

            return new Gson().toJsonTree(productService.getProducts());
        });

        post("/product", (request, response) -> {
            response.type("application/json");

            Product p = new Gson().fromJson(request.body(), Product.class);
            productService.addProduct(p);


            response.status(200);
            response.body("");
            return response;
        });

        delete("/product", (request, response) -> {
            response.type("application/json");

            Product p = new Gson().fromJson(request.body(), Product.class);
            productService.deleteProduct(p);

            response.status(200);
            response.body("");
            return response;
        });

        post("/shutdown", (request, response) -> {
            Spark.stop();
            Spark.awaitStop();
            response.status(200);
            System.exit(1);
            return response;
        });

        TimerTask timerTask = new TimerTask() {
            public void run() {
                System.out.println("TimerTask executing counter is: " + counter);
                JSONObject body = CoffeeMachine.parseData(productService.getProducts());
                System.out.println(body);
                ((HttpRequestWithBody)(Unirest.post(serverURL + ":" + serverPort + "/data")
                        .header("accept", "application/json"))
                        .header("Content-Type", "application/json"))
                        .body(body).asJson();
                counter++;
            }
        };
        Timer timer = new Timer("MyTimer");
        timer.scheduleAtFixedRate(timerTask, (long)freq, (long)freq);
    }

    private static void readConf() {
        Properties configFile = new java.util.Properties();
        try {
            FileInputStream file;
            String path = "./agent.cfg";
            file = new FileInputStream(path);
            configFile.load(file);
            file.close();
            agentPort = Integer.parseInt(configFile.getProperty("agentPort"));
            serverURL = configFile.getProperty("serverURL");
            serverPort = Integer.parseInt(configFile.getProperty("serverPort"));
            freq = Integer.parseInt(configFile.getProperty("frequency"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static JSONObject parseData(Collection<Product> products) {
        JSONObject body = new JSONObject();
        JSONArray productsMap = new JSONArray();
        JSONObject bodyPart;
        for(Product p : products) {
            productsMap
                    .put(new JSONObject()
                            .put(p.getProductName(), p.getQuantity()));
        }
        body.put("products", productsMap);
        body.put("port", agentPort);
        body.put("version", "1.1");

        return body;
    }

    private static void startAgent() {

    }
}
