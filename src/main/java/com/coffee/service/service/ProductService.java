package com.coffee.service.service;

import com.coffee.service.model.Product;

import java.util.Collection;

public interface ProductService {
    void addProduct(Product product);
    Collection<Product> getProducts();
    void deleteProduct(Product product);
    boolean productExist(String productName);
}
