package com.coffee.service.service;

import com.coffee.service.model.Product;

import java.util.Collection;
import java.util.HashMap;

public class ProductServiceImpl implements ProductService {
    private HashMap<String, Product> productMap;

    public ProductServiceImpl() {
        productMap = new HashMap<>();
    }


    @Override
    public void addProduct(Product product) {
        if (productExist(product.getProductName())) {
            Product p = productMap.get(product.getProductName());
            // Gestion valeur négative
            p.setQuantity(product.getQuantity() > 0 ? p.getQuantity() + product.getQuantity() : p.getQuantity());
        } else {
            product.setQuantity(product.getQuantity() > 0 ? product.getQuantity() : 0);

            productMap.put(product.getProductName(), product);
        }
    }

    @Override
    public Collection<Product> getProducts() {
        return productMap.values();
    }

    @Override
    public void deleteProduct(Product product) {
        if (productExist(product.getProductName())) {
            Product p = productMap.get(product.getProductName());
            // Gestion valeur négative
            Integer newQuantity = product.getQuantity() > 0 ? p.getQuantity() - product.getQuantity() : p.getQuantity();
            // Gestion résultat négatif
            p.setQuantity(newQuantity > 0 ? newQuantity : 0);
        }
    }

    @Override
    public boolean productExist(String productName) {
        return productMap.containsKey(productName);
    }
}